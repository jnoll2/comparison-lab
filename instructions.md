---
author: 'Dr. John Noll'
course: 7COM1079
date: '\today'
institute: University of Hertfordshire
module: 7COM1079
subtitle: '7COM1079 -- Team Research and Development Project'
term: 'fall-19'
title: Significance
---

Overview
========

We're going to compare teams from the [NBA player
statistics](http://jnoll.nfshost.com/7COM1079-fall-19/topics/Correlation/Seasons_Stats.csv)
dataset.

Load and set-up the dataset
---------------------------

Once again you have to import the dataset as a CSV file, then ensure the
variables of interest are recognized correctly.

This week we are interested in *Year*, *Tm* (team), *FG\_A* (field goals
scored), and *3P* (three point shots scored).

Display a test plot
-------------------

It's a good idea to display a test plot of some aspect of your dataset,
to be sure it loaded correctly.

Try a histogram of *FG\_A* in 1972.

To do this, you first have to *select* a subset of the data: the rows
representing scores in 1972:

1.  From the "Data" menu, select "Select Cases..."

    ![Select cases](spss-select-cases.png)\

2.  In the "Select Cases" dialog, select the "If condition is satisfied"
    radio button, then click the "If" button:

    ![Select cases](spss-select-cases-dialog.png)\

3.  Specify 'Year = 1972'

    ![Select 1972](spss-select-cases-condition.png)\

    You should see a data view with cases for years other than 1972
    crossed out:

    ![Select 1972 result](spss-select-cases-output.png)\

4.  Now, use the "Legacy Dialogs" to create a histogram of the data
    subset.

1972 Lakers vs league
---------------------

In 1972 the LA Lakers won the NBA championship after a record-breaking
season in which they won 33 games in a row.

Did the Lakers score more on average than the rest of the league as a
whole in 1972?

Unfortunately SPSS does not provide a way to perform a $Z$-test (!) so
we have to use a "one sample" $t$-test instead.

1.  First, you have to compute the mean for *all* teams in 1972. You
    know how to do this already. (Hint: it's a descriptive statistic.)

2.  Next, you must select the cases in 1972 that are related to the
    Lakers ("LAL" is for "Los Angeles Lakers"):

    ![Select cases: LAL](spss-select-cases-lal.png)\

3.  From the "Analyze" menu, select "Compare Means" then "One-Sample T
    test..."

    ![One-Sample T test](spss-one-t-test.png)\

4.  Use *FG\_A* as the variable to analyze:

    ![One-Sample variable](spss-one-t-test-dialog.png)\

    Also specify the previously computed mean as the "Test Value."

5.  What does it mean?

    ![One-Sample output](spss-one-t-test-output.png)\

6.  Repeat, but this time with the Celtics of 1974.

1972 Lakers vs 1972 NY Knicks
-----------------------------

In 1972 the Lakers beat the Knicks for the NBA championship.

Did the lakers score more on average than the Knicks during the 1972
season?

1.  Reverse the case restriction to "LAL" you created above by unticking
    the "If condition..." radio button on the "Select Cases" dialog.

2.  From the "Analyze" menu, select "Compare Means" then "Independent
    Samples T Test"

    ![Two-sample T test](spss-two-t-test.png)\

3.  Specify the variable to compare:

    ![Two-sample T test: select variable](spss-two-t-test-dialog.png)\

4.  Define the "groups" to compare:

    ![Two-sample T test: define
    groups](spss-two-t-test-define-groups.png)\

5.  What does it mean?

    ![Two-Sample output](spss-two-t-test-output.png)\

6.  Repeat, but this time with the Celtics vs. Milwaukee of 1974.

2017 Lakers vs 2017 Celtics
---------------------------

Was there a difference in proportion of 3-point shots made by each team
in the 2017[^1] season?

Unfortunately SPSS doesn't provide a means to compute Chi-square
statistics directly on a contingency table; SPSS prefers to create the
contingency table from scratch based on raw frequency data. To do this,
we would need a record of *every* shot made by each player in *every*
game.

As such, we need to simulate frequency data by creating our own summary
dataset, and *weighting* the frequency column ("BOS" is for "Boston
Celtics," pronounced "Sell Ticks" in North America):

  ShotType   Team   Count
  ---------- ------ -------
  FG         BOS    3168
  TP         BOS    985
  FG         LAL    3224
  TP         LAL    730

1.  Create a new dataset with three variables (columns):

    1.  ShotType (categorical).
    2.  Team (categorical).
    3.  Count (interval).

![Celtics vs Lakers 3pt](spss-crosstab-data.png)\

1.  *Weight* the *Count* variable: from the *Data* menu, select *Weight
    Cases*:

    ![Weight cases](spss-crosstab-data-weight-cases.png)\

2.  Select the "Weight cases by" radio button, with *Count* as the
    "Frequency variable":

    ![Weight *Count*](spss-crosstab-weight-cases-dialog.png)\

3.  Now, create a crosstabulation table ("Crosstab"):

    ![Define crosstab](spss-crosstab-define.png)\

4.  What does it mean?

    ![Crosstab output](spss-crosstab-output.png)\

5.  Repeat, but this time with the Celtics vs. Lakers in 2010.

[^1]: The NBA didn't have three-point goals until 1979.
